import React from 'react';
import classnames from 'classnames';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';


const creator = [
  {
    title: <>Product designer</>,
    imageUrl: 'img/projectdesigner.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name%5B%5D=Creators%3A%3AProduct+Designer',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  },
  {
    title: <>Project Manager</>,
    imageUrl: 'img/project_manager.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name%5B%5D=Creators%3A%3AProject+Manager',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  },
  {
    title: <>Manufacturer</>,
    imageUrl: 'img/manufacturer.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name%5B%5D=Creators%3A%3AManufacturer',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  },
  {
    title: <>Electronics Engineer</>,
    imageUrl: 'img/undraw_circuit_board_4c4d.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name%5B%5D=Creators%3A%3AElectronics+Engineer',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  },
  {
    title: <>Mechanical Engineer</>,
    imageUrl: 'img/engineer.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name%5B%5D=Creators%3A%3AMechanical+Engineer',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  },
  {
    title: <>Embedded Software Engineer</>,
    imageUrl: 'img/undraw_Firmware_jw6u.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name%5B%5D=Creators%3A%3AEmbedded+Software+Engineer',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  },
  {
    title: <>Gitlab Expert</>,
    imageUrl: 'img/gitlab-logo-gray-stacked-rgb.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name%5B%5D=Creators%3A%3AGitlab+Expertise',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  },  
];

const outreach = [
  {
    title: <>Distributor</>,
    imageUrl: 'img/undraw_deliveries_131a.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name=Marketing+%26+Outreach%3A%3ADistributor',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  },
  {
    title: <>Content creator</>,
    imageUrl: 'img/undraw_social_girl_562b.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name=Marketing+%26+Outreach%3A%3AContent+Creator',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  },
  {
    title: <>Social Media Expert</>,
    imageUrl: 'img/undraw_social_expert_07r8.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name=Marketing+%26+Outreach%3A%3ASocial+Media+Expert',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  },
  {
    title: <>Media</>,
    imageUrl: 'img/socialmedia.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name=Marketing+%26+Outreach%3A%3AMedia',
    description: (
      <> You will have to pick some skills and get trained before contributing
      </>
    ),
  },   
];

const funding = [
  {
    title: <>Investor</>,
    imageUrl: 'img/undraw_invest_88iw.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name[]=Funding%3A%3AInvestor',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  },
  {
    title: <>Incubator</>,
    imageUrl: 'img/undraw_fatherhood_7i19.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name[]=Funding%3A%3AIncubator',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  },
  {
    title: <> Fund Raiser </>,
    imageUrl: 'img/undraw_wallet_aym5.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name[]=Funding%3A%3AFund%20Raiser',
    description: (
      <> You will have to pick some skills and get trained before contributing
      </>
    ),
  },   
];

const experts = [
  {
    title: <>Certifications & Specs</>,
    imageUrl: 'img/undraw_certification_aif8.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name[]=Industry%20Experts%3A%3ACertification%20%26%20Specs',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  },
  {
    title: <>Biomedical Engineer</>,
    imageUrl: 'img/undraw_medicine_b1ol.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name=Industry+Experts%3A%3ABiomedical+Engineer',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  },
];

const enthu = [
  {
    title: <>Student</>,
    imageUrl: 'img/student.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name%5B%5D=Enthusiast%3A%3AStudent',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  },
  {
    title: <>Enthusiast</>,
    imageUrl: 'img/undraw_maker_launch_crhe.svg',
    issueURL: 'https://gitlab.com/iotiotdotin/open-ventilator/openventi/-/issues?label_name%5B%5D=Enthusiast%3A%3AEnthusiast',
    description: (
      <> Pick up any open tasks and you can start contributing.
      </>
    ),
  }, 
];

function Feature({imageUrl, title, issueURL, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={classnames('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <div className="container">
        <h3>{title}</h3>
        <p>{description}</p>
        <div className={styles.buttons}>
          <a href={issueURL}>
            <div
              className={classnames(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}>
              Contribute
            </div>
          </a>
        </div>
      </div>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`${siteConfig.title}`}
      description="Open source Ventilator <head />">
      <header className={classnames('hero hero--primary', styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">{siteConfig.title}</h1>
          <p className="hero__subtitle">{siteConfig.tagline}</p>
        </div>
      </header>
      <main>
        <div className={classnames('hero hero--primary-dark', styles.heroBanner)}>
          <div className="container">
            <h1 className="hero__title">Want to contribute?</h1>
            <p className="hero__subtitle">Please pick one Role that suits you, if you can do multiple things, you will find different tasks under different roles.</p>
          </div>
        </div>
        {creator && creator.length && (
          <section className={styles.creator}>
            <div className={classnames('hero hero--primary-darkest', styles.headerBanner)}>
              <div className="container">
                <h1 className="hero__title">Creators</h1>
              </div>
            </div>
            <div className="container">
              <div className="row">
                {creator.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
        {outreach && outreach.length && (
          <section className={styles.outreach}>
            <div className={classnames('hero hero--primary-darkest', styles.headerBanner)}>
              <div className="container">
                <h1 className="hero__title">Marketing & Outreach</h1>
              </div>
            </div>
            <div className="container">
              <div className="row">
                {outreach.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
        {funding && funding.length && (
          <section className={styles.outreach}>
            <div className={classnames('hero hero--primary-darkest', styles.headerBanner)}>
              <div className="container">
                <h1 className="hero__title">Funding</h1>
              </div>
            </div>
            <div className="container">
              <div className="row">
                {funding.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
        {experts && experts.length && (
          <section className={styles.outreach}>
            <div className={classnames('hero hero--primary-darkest', styles.headerBanner)}>
              <div className="container">
                <h1 className="hero__title">Industry Experts</h1>
              </div>
            </div>
            <div className="container">
              <div className="row">
                {experts.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
        {enthu && enthu.length && (
          <section className={styles.outreach}>
            <div className={classnames('hero hero--primary-darkest', styles.headerBanner)}>
              <div className="container">
                <h1 className="hero__title">Enthusiasts</h1>
              </div>
            </div>
            <div className="container">
              <div className="row">
                {enthu.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
        <div className={classnames('hero hero--primary-dark', styles.heroBanner)}>
          <div className="container">
            <h1 className="hero__title">Excited to contribute?</h1>
            <p className="hero__subtitle">Get started by Reading our contributer guide</p>
            <div className={styles.buttons}>
              <a href="https://gitlab.com/iotiotdotin/open-ventilator/welcome-to-openventi/-/wikis/home">
                <div
                  className={classnames(
              		'button button--outline button--secondary button--lg',
              		styles.indexCtas,
                  )}>
                  Read Contributer Guide
                </div>
              </a>
            </div>
          </div>
        </div>
      </main>
    </Layout>
  );
}

export default Home;
